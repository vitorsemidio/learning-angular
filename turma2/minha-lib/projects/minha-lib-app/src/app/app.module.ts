import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MinhaLibComponent } from 'minha-lib';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    MinhaLibComponent
  ],
  providers: [],
  exports: [MinhaLibComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
