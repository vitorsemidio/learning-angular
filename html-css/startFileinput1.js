function checarProgresso() {
  $(".divForm").hide();
  $(".divFormLocalizar").hide();
  $(".divProgress").show();

  campoUnico = $("#campoUnico").val();

  $.ajax({
    url: "progresso",
    method: "GET",
    data: {
      campoUnico: campoUnico
    },
    success: function(jsonGerado) {
      if (jsonGerado.situacaoPagina != "") {
        objectSituacaoPagina = JSON.parse(jsonGerado.situacaoPagina);

        console.log(objectSituacaoPagina);

        $(".divProgress b").text(jsonGerado.progresso + "%");

        if (Object.keys(objectSituacaoPagina).length != 0) {
          $(".btnCancel").show();
          for (let i = 0; i < Object.keys(objectSituacaoPagina).length; i++) {
            if (objectSituacaoPagina[i + 1] != "Done") {
              if ($(".divPaginasOcred ul ." + (i + 1)).length == 0) {
                $(".divPaginasOcred ul").append(
                  '<li class="' +
                    (i + 1) +
                    '">' +
                    "Página " +
                    (i + 1) +
                    " " +
                    objectSituacaoPagina[i + 1] +
                    "</li>"
                );
              }
            } else if ($(".divPaginasOcred ul ." + (i + 1)).length == 0)
              $(".divPaginasOcred ul").append(
                '<li class="' +
                  (i + 1) +
                  '">' +
                  "Página " +
                  (i + 1) +
                  " " +
                  objectSituacaoPagina[i + 1] +
                  "</li>"
              );
            else
              $(".divPaginasOcred ul ." + (i + 1)).text(
                "Página " + (i + 1) + " " + objectSituacaoPagina[i + 1]
              );
          }
        }
      }

      if (jsonGerado.progresso == 100) {
        clearTimeout(timeOutVar);
        $(".btnCancel").hide();
        $(".anchorResultado").attr("href", jsonGerado.url);
        $(".divResultado").show();
      }
    }
  });
  var timeOutVar = setTimeout(checarProgresso, 4000);
}

function requisicaoPost() {
  var formData = new FormData($("#formOCR")[0]);

  $.ajax({
    url: "upload",
    enctype: "multipart/form-data",
    data: formData,
    method: "POST",
    contentType: false,
    processData: false
  });
}

function requisicaoDelete() {
  codigoProcessamento = $("#campoUnico").val();

  $.ajax({
    url: "encerrar/" + codigoProcessamento,
    method: "DELETE",
    success: function() {
      location.reload();
    }
  });
}

function verificarProgressoExistente(valorUnico) {
  $.ajax({
    url: "verificar-processamento",
    data: {
      campoUnico: valorUnico
    },
    method: "GET",
    statusCode: {
      200: function() {
        $("#campoUnico").val(valorUnico);
        $(".divProgress div").addClass("loader");
        $(".divFormLocalizar").hide();
        checarProgresso();
      },
      404: function() {
        alert("Código não encontrado!");
        $("#campoCodigoUnico").val("");
      }
    }
  });
}

$(document).ready(function() {
  let data = new Date();
  let salt = Math.ceil( Math.random() * 1000);

  let valorUnico =
    data.getDay() +
    "" +
    data.getMonth() +
    "" +
    data.getFullYear() +
    "" +
    data.getHours() +
    "" +
    data.getMinutes() +
    "" +
    data.getSeconds() +
    "" +
    data.getMilliseconds() +
    "" +
    salt;

  $("#campoUnico").val(valorUnico);

  $(".btnCancel").hide();
  $(".divResultado").hide();
  $(".divProgress").hide();
});

$(".btnOCR").on("click", function() {
  if ($("#formOCR")[0].checkValidity() == true) {
    $("#myAlert").text("Código do processamento: " + $("#campoUnico").val());
    $("#myAlert").addClass("alert alert-info");

    requisicaoPost();

    $(".divProgress div").addClass("loader");
    checarProgresso();
  } else {
    alert(
      "Formato de e-mail incorreto, ou adicione um documento para o processamento"
    );
  }
});

$(".btnCancel").on("click", function() {
  requisicaoDelete();
});

$(".btnLocalizar").on("click", function() {
  verificarProgressoExistente($("#campoCodigoUnico").val());
});
