var Oidc = window.Oidc,
    UserManager = Oidc.UserManager;

var manager = new UserManager();

manager.signinSilentCallback()
    .catch((err) => {
        console.error('signinSilentCallback error:', err);
    });