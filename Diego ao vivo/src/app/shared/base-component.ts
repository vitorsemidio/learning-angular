import { AlertService, BlockUi } from 'ngx-ui-hero';

export abstract class BaseComponent {

    constructor(public alert: AlertService) {
    }

    tratarErro(erro: any) {
        if (!erro) return;

        if (erro.unauthorized) {
            this.alert.warning('Atenção', 'Acesso negado!');
            return;
        }

        this.alert.error(erro.title, erro.message);
    }

    carregar(promises: Array<Promise<void>>, blockUi: BlockUi, message?: string): Promise<void> {
        blockUi.start(message || 'Carregando...');

        return new Promise<void>((resolve, reject) => {
            Promise.all(promises)
                .then(() => {
                    resolve();
                    blockUi.stop();
                })
                .catch(error => {
                    reject(error);
                    this.tratarErro(error);
                    blockUi.stop();
                });
        });
    }

}