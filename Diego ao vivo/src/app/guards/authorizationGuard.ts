import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import { AuthService } from '../services/auth.service';

@Injectable()
export class AuthorizationGuard implements CanActivate {

    constructor(
        private router: Router,
        private authService: AuthService
    ) { }

    public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {    
        let self = this;
        let isloggedIn = this.authService.isLoggedIn();
        isloggedIn.subscribe((loggedin) => {
            if(!loggedin){
                self.router.navigate(['/login']);
            } else if (
                this.authService.user != null 
                && this.authService.user.profile != null 
                && (!this.authService.user.profile.ido || !this.authService.user.profile.idusuarioscap)
            ) {
                self.router.navigate(['/unauthorized']);
            }
        });
        return isloggedIn;
    }
}
