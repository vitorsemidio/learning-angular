import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing.module';
import { AuthorizationGuard } from './guards/authorizationGuard';
import { HomeComponent } from './home/home.component';
import { LayoutModule } from './layout/layout.module';
import { NotFoundComponent } from './not-found/not-found.component';
import { ApiService } from './services/api.service';
import { AuthService } from './services/auth.service';
import { ImagensService } from './services/imagens.service';
import { ResponseHandlerService } from './services/response-handler.service';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NotFoundComponent,
  ],
  imports: [
    BrowserModule,
    LayoutModule,
    AppRoutingModule,
    SharedModule,
    HttpModule
  ],
  providers: [
    ApiService,
    AuthService,
    AuthorizationGuard,
    ImagensService,
    ResponseHandlerService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
