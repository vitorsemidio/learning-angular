import { Observable } from 'rxjs';

import { ApiService } from './api.service';

export abstract class BaseCrudService<T> {
    constructor(
        public path: string,
        public api: ApiService
    ) {}

    obterTodos(busca?: string): Observable<T[]> {
        return this.api.get(`${this.path}?q=${busca}`);
    }
    obterPorId(id: string): Observable<T> {
        return this.api.get(`${this.path}/${id}`);
    }
    criar(obj: T): Observable<void> {
        return this.api.post(this.path, obj);
    }
    atualizar(obj: T, id: string): Observable<void> {
        return this.api.put(`${this.path}/${id}`, obj);
    }
    excluir(id: string): Observable<void> {
        return this.api.delete(`${this.path}/${id}`);
    }
}