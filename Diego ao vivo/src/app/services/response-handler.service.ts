import { Observable, throwError } from 'rxjs';

import { Injectable } from '@angular/core';
import { Response, ResponseContentType } from '@angular/http';

@Injectable()
export class ResponseHandlerService {

    constructor() { }

    handleSuccess(response: Response, responseType: ResponseContentType): any {
        switch (responseType) {
            case ResponseContentType.Json:
                return response.json().data;
            case ResponseContentType.Blob:
                return response.blob();
            default:
                return response.text();
        }
    }
    handleError(response: Response): Observable<any> {
        let error: any;

        if (response.status == 401) {
            error = {
                unauthorized: true
            };
        } else if (response.status === 400 || response.status === 404) {
            const result = response.json();

            if (result && result.errors) {
                let errosDaApi = '';

                for (let i = 0; i < result.errors.length; i++) {
                    errosDaApi += `<p>${result.errors[i]}</p>`;
                }

                error = {
                    title: 'TCE-RJ / Validação',
                    message: errosDaApi
                };
            }
        } else {
            error = this.getDefaultErrorObject();
        }

        return throwError(error);
    }

    private getDefaultErrorObject(): any {
        return {
            title: 'TCE-RJ',
            message: 'Encontramos uma falha interna ao realizar esta operação.'
        };
    }

}
