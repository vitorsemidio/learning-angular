import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

import { ResponseContentType } from '@angular/http';
import { ApiService } from './api.service';

@Injectable()
export class ImagensService {

    baseUrl = environment.servicoImagensApiBaseUrl;

    constructor(private api: ApiService) { }

    ObterImagemPorCpf(cpf: string): Observable<any> {
        return this.api.get(this.baseUrl + `ImagemServidorBase64/${cpf}`, null, null, true, ResponseContentType.Text);
    }

}