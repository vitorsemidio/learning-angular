export class ComentarioModel {
  id: string;
  texto: string;
  dataCadastro: Date;
  dataUltimaAlteracao?: Date;
}
