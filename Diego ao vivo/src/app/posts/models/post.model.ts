import { ComentarioModel } from './comentario.model';
export class PostModel {
    id: string;
    titulo: string;
    descricao: string;
    dataCadastro: Date;
    dataUltimaAlteracao?: Date;

    comments: Array<ComentarioModel>
}
