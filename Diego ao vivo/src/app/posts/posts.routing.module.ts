import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PostDetalheComponent } from './post-detalhe/post-detalhe.component';
import { PostsComponent } from './posts.component';

const routes: Routes = [
    { path: '', component: PostsComponent },
    { path: 'novo', component: PostDetalheComponent },
    { path: ':id/edicao', component: PostDetalheComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: [],
})
export class PostsRoutingModule { }