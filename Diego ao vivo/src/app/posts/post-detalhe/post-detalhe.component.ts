import { AlertService, BlockUi, DataGridColumnModel } from 'ngx-ui-hero';
import { finalize } from 'rxjs/operators';
import { BaseComponent } from 'src/app/shared/base-component';

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { PostModel } from '../models/post.model';
import { PostsService } from '../services/posts.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ComentarioCadastroModalComponent } from "../comentario-cadastro-modal/ComentarioCadastroModalComponent";
import { ComentarioModel } from '../models/comentario.model';

@Component({
  selector: 'app-post-detalhe',
  templateUrl: './post-detalhe.component.html',
  styleUrls: ['./post-detalhe.component.scss']
})
export class PostDetalheComponent extends BaseComponent implements OnInit {
  blockUi = new BlockUi();
  post: PostModel;
  comentarios: Array<ComentarioModel>;

  columns: Array<DataGridColumnModel> = [
    {
      caption: 'Texto',
      data: 'texto',
    },
    {
      caption: 'Criado em',
      data: 'dataCadastro',
    },
  ];

  constructor(
    private service: PostsService,
    public alert: AlertService,
    private router: Router,
    private route: ActivatedRoute,
    private modalService: BsModalService
  ) {
    super(alert);
    this.post = new PostModel();
  }

  ngOnInit() {
    this.post.id = this.route.snapshot.params.id;

    this.carregar([this.carregarCadastro()], this.blockUi);
  }

  salvar() {
    this.alert.question('Confirmação', 'Você deseja realmente salvar?', () => {
      if (this.post.id) {
        this.atualizar();
      } else {
        this.criar();
      }
    });
  }

  private carregarCadastro(): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      if (!this.post.id) {
        resolve();
      } else {
        this.service.obterPorId(this.post.id)
          .subscribe(
            result => {
              this.post = result;
              resolve();
            },
            error => reject(error)
          );
      }
    });
  }
  private criar() {
    this.blockUi.start('Criando...');

    this.service.criar(this.post)
      .pipe(finalize(() => this.blockUi.stop()))
      .subscribe(
        result => {
          this.alert.success('TCE-RJ', 'Post criado com sucesso!');
          this.router.navigate(['/posts']);
        },
        error => this.tratarErro(error)
      );
  }
  private atualizar() {
    this.blockUi.start('Atualizando...');

    this.service.atualizar(this.post, this.post.id)
      .pipe(finalize(() => this.blockUi.stop()))
      .subscribe(
        result => {
          this.alert.success('TCE-RJ', 'Post atualizado com sucesso!');
          this.router.navigate(['/posts']);
        },
        error => this.tratarErro(error)
      );
  }

  abrirCriacaoOuEdicaoDeComentario(index?: number) {
    let modalRef = this.modalService.show(ComentarioCadastroModalComponent, {
      class: 'modal-lg',
      initialState: {
        postId: this.post.id,
        comment: index >= 0 ? this.comentarios[index] : null
      }
    });

    modalRef.content.salvouEmMemoria.subscribe(data => {
      // console.log(data);
      if (index >= 0) {
        this.comentarios[index] = data;
      } else {
        if (!this.comentarios) {
          this.comentarios = new Array<ComentarioModel>();
        }

        this.comentarios.push(data);
      }
    });
  }

  excluirComentario(index: number) {
    if (this.post.id) {

    } else {
      this.comentarios.splice(index, 1);
    }
  }

}
