import { AlertService, BlockUi, DataGridColumnModel } from 'ngx-ui-hero';
import { finalize } from 'rxjs/operators';

import { Component, OnInit } from '@angular/core';

import { BaseComponent } from '../shared/base-component';
import { PostModel } from './models/post.model';
import { PostsService } from './services/posts.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent extends BaseComponent implements OnInit {  
  blockUi = new BlockUi();
  busca: string = '';
  data: Array<any>;
  columns: Array<DataGridColumnModel> = [
    {
      caption: 'Título',
      data: 'titulo',
    },
    {
      caption: 'Descrição',
      data: 'descricao',
    },
  ];

  constructor(
    public alert: AlertService,
    private service: PostsService
  ) {
    super(alert);
  }

  ngOnInit() {
    this.carregarTela();
  }

  carregarTela() {
    this.carregar([this.carregarPosts()], this.blockUi);
  }

  excluir(row: PostModel) {
    this.alert.question('Confirmação', 'Você deseja realmente excluir este Post?', () => {
      this.blockUi.start('Excluindo...');

      this.service.excluir(row.id)
        .pipe(finalize(() => this.blockUi.stop()))
        .subscribe(
          result => {
            this.alert.success('TCE-RJ', 'O Post foi excluído com sucesso!');
            this.carregarTela();
          },
          error => this.tratarErro(error)
        );
    });
  }

  carregarPosts(): Promise<void> {
    return new Promise<void>((resolve, reject) => {

      this.service.obterTodos(this.busca)
        .subscribe(
          (data: any) => {
            this.data = data;
            resolve();
          },
          error => reject(error)
        );

    });
  }

}
