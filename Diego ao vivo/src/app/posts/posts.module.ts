import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { SharedModule } from '../shared/shared.module';
import { PostsComponent } from './posts.component';
import { PostsRoutingModule } from './posts.routing.module';
import { PostsService } from './services/posts.service';
import { PostDetalheComponent } from './post-detalhe/post-detalhe.component';
import { ComentarioCadastroModalComponent } from "./comentario-cadastro-modal/ComentarioCadastroModalComponent";

@NgModule({
  declarations: [PostsComponent, PostDetalheComponent, ComentarioCadastroModalComponent],
  imports: [
    CommonModule,
    FormsModule,
    PostsRoutingModule,
    SharedModule
  ],
  providers: [
    PostsService
  ],
  entryComponents: [ComentarioCadastroModalComponent]
})
export class PostsModule { }
