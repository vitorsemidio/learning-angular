import { ComentarioModel } from './../models/comentario.model';
import { Component, OnInit, EventEmitter } from '@angular/core';
import { BlockUi, AlertService } from 'ngx-ui-hero';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { PostsService } from '../services/posts.service';
import { finalize } from 'rxjs/operators';
import { BaseComponent } from '../../shared/base-component';

@Component({
  selector: 'app-comentario-cadastro-modal',
  templateUrl: './comentario-cadastro-modal.component.html',
  styleUrls: ['./comentario-cadastro-modal.component.scss']
})
export class ComentarioCadastroModalComponent implements OnInit {
  comentario: ComentarioModel;
  postId: string;
  blockUi = new BlockUi;
  editando: boolean;
  salvouEmMemoria = new EventEmitter<ComentarioModel>();

  constructor(
    private modalService: BsModalService,
    private modalRef: BsModalRef,
    private service: PostsService,
    private alert: AlertService
  ) {
  }

  ngOnInit() {
    this.postId = this.modalService.config.initialState['postId'];

    let comentarioQueVeioDaTela = this.modalService.config.initialState['comment'];
    this.editando = comentarioQueVeioDaTela != null && comentarioQueVeioDaTela != undefined;

    if (comentarioQueVeioDaTela) {
      this.comentario = Object.assign({}, comentarioQueVeioDaTela);
    } else {
      this.comentario = new ComentarioModel();
    }
  }

  salvar() {
    if (this.editando) {
      this.atualizar();
    } else {
      this.criar();
    }
  }

  private criar() {
    if (this.postId) {
      this.blockUi.start('Criando...');

      this.service.AdicionarComentario(this.postId, this.comentario)
        .pipe(finalize(() => this.blockUi.stop()))
        .subscribe(
          result => {
            this.alert.success('texto', 'Comentario adicionado com sucesso');
            this.modalRef.hide();


            this.comentario = result;
          }
        )

    } else {
      this.salvouEmMemoria.emit(this.comentario);
      this.modalRef.hide();
    }
  }

  private atualizar() {
    if (this.postId) {
      if (this.postId) {

        this.blockUi.start('Atualizando...');

        this.service.AdicionarComentario(this.postId, this.comentario)
          .pipe(finalize(() => this.blockUi.stop()))
          .subscribe(
            result => {
              this.alert.success('Atualizacao', 'Comentario atualizado com sucesso');
              this.modalRef.hide();

              this.comentario = result;
            }
          )
    } else {
      this.salvouEmMemoria.emit(this.comentario);
      this.modalRef.hide();
    }
  }



}
}
