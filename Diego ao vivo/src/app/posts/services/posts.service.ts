import { Injectable } from "@angular/core";

import { ApiService } from "../../services/api.service";
import { BaseCrudService } from "../../services/base-crud-service";
import { PostModel } from "../models/post.model";
import { ComentarioModel } from "../models/comentario.model";

@Injectable()
export class PostsService extends BaseCrudService<PostModel> {
  constructor(public api: ApiService) {
    super("posts", api);
  }

  // obterComentariosPorPostId(postId: string) {
  //   return this.api.get(`posts/${postId}/comments`);
  // }

  // obterComentarioPorId(postId: string, comentarioId: string) {
  //   return this.api.get(`posts/${postId}/comments/${comentarioId}`);
  // }

  // adicionarComentarios(postId: string, comentario: ComentarioModel) {
  //   return this.api.post(`posts/${postId}/comments/${comentarioId}`);
  // }
  // atualizarComentarios(postId: string, comentario: ComentarioModel) {

  // }
  // excluirComentarios(postId: string) {

  // }

  ObterComentariosPorPostId(postId: string) {
    return this.api.get(`posts/${postId}/comments`);
  }
  ObterComentarioPorId(postId: string, comentarioId: string) {
    return this.api.get(`posts/${postId}/comments/${comentarioId}`);
  }
  AdicionarComentario(postId: string, comentario: ComentarioModel) {
    return this.api.post(`posts/${postId}/${comentario.id}`, comentario);
  }
  AtualizarComentario(postId: string, comentario: ComentarioModel) {
    return this.api.put(
      `posts/${postId}/comments/${comentario.id}`,
      comentario
    );
  }
  ExcluirComentario(postId: string, comentarioId: string) {
    return this.api.delete(`posts/${postId}/comments/${comentarioId}`);
  }
}
