const SMA256 = require('crypto-js/sha256')


class Block {
    constructor(index, timestamp, data, previousHash = '') {
        this.index = index;
        this.timestamp = timestamp;
        this.data = data;
        this.previousHash = previousHash;
        this.hash = this.calculateHash();
        this.nonce = 0;
    }

    calculateHash() {
        return SMA256(this.index + this.previousHash + this.timestamp + JSON.stringify(this.data + this.nonce)).toString();
    }

    mineBlock(difficulty) {
        // strings with zero has the same length of the difficulty
        while(this.hash.substring(0, difficulty) !== Array(difficulty + 1).join("0")) {
            this.nonce++;
            this.hash = this.calculateHash();
        }

        console.log("Block mined " + this.hash);
    }
}

class BlockChain {
    constructor() {
        this.chain = [this.createGenesisBlock()];
        this.difficulty = 2;
    }

    createGenesisBlock() {
        return new Block(0, "01/01/2019", "Genesis block", "0");
    }

    getLatestBlock() {
        return this.chain[this.chain.length - 1];
    }

    addBlock(newBlock) {
        newBlock.previousHash = this.getLatestBlock().hash;
        newBlock.mineBlock(this.difficulty);
        this.chain.push(newBlock);
    }

    isChainValid() {
        for (let i = 1; i < this.chain.length; i++) {
            const currentBlock = this.chain[i];
            const previousBlock = this.chain[i-1];

            if (currentBlock.hash !== currentBlock.calculateHash()) {
                console.log(i, 'fun');
                return false;
            }

            if (currentBlock.previousHash !== previousBlock.hash) {
                console.log(i, 'previeow');
                console.log(currentBlock.previousHash, previousBlock.hash);
                return false;
            }
        }

        return true;
    }
}

let savjeeCoin = new BlockChain();
console.log('1')
savjeeCoin.addBlock(new Block(1, "01/01/2019", "Genesis block", { amount: 4 }));
console.log('2')
savjeeCoin.addBlock(new Block(2, "01/02/2019", "Genesis block", { amount: 10 }));

// console.log(JSON.stringify(savjeeCoin, null, 4));

// console.log('Blockchain valid? ', savjeeCoin.isChainValid());

// savjeeCoin.chain[1].data = { amount: 100 };

// console.log('Blockchain valid? ', savjeeCoin.isChainValid());