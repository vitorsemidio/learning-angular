import { Component } from '@angular/core';
import { ConfigService, Config } from './config.service';

@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  providers: [ ConfigService ],
  styles: ['.error {color: red;}']
})
export class ConfigComponent {
  config: Config;
  headers: string[];
  error: any;

  constructor(
    private configService: ConfigService
  ) { }

  showConfig() {
    this.configService.getConfig().subscribe(
      (data: Config) => this.config = {
        // v1 heroesUrl: data['heroesUrl'], textfile: data['textfile']
        ...data
      },
      error => this.error = error
    );
  }

  showConfigResponse() {
    this.configService.getConfigResponse()
      // resp is of type `HttpResponse<Config>`
      .subscribe(resp => {
        // display its headers
        const keys = resp.headers.keys();
        this.headers = keys.map(key =>
          `${key}: ${resp.headers.get(key)}`);

        // access the body directly, which is typed as `Config`.
        this.config = { ... resp.body };
      });
  }


}
