import { DownloaderService } from './downloader.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-downloader',
  templateUrl: './downloader.component.html'
})
export class DownloaderComponent implements OnInit {
  contents: string;

  constructor(
    private downloaderService: DownloaderService
  ) { }

  ngOnInit() {
  }

  download() {
    this.downloaderService.getTextFile('assets/textfile.txt')
      .subscribe(results => this.contents = results);
  }

}
