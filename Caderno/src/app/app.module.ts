import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutModule } from './layout/layout.module';
import { ProjectModule } from './project/project.module';
import { FormsModule } from '@angular/forms';
import { CookieService } from 'angular2-cookie/core';
import { NgxWebstorageModule, LocalStorageService, SessionStorageService } from 'ngx-webstorage';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    LayoutModule,
    NgxWebstorageModule.forRoot(),
    ProjectModule
  ],
  providers: [
    CookieService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
