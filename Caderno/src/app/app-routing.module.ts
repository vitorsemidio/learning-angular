import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StoreDataComponent } from './project/store-data/store-data.component';
import { BlockchainMenuComponent } from './project/blockchain/blockchain-menu/blockchain-menu.component';
import { BlockchainViewerComponent } from './project/blockchain/pages/blockchain-viewer/blockchain-viewer.component';
import { CreateTransactionComponent } from './project/blockchain/pages/create-transaction/create-transaction.component';
import { PendingTransactionsComponent } from './project/blockchain/pages/pending-transactions/pending-transactions.component';
import { SettingsComponent } from './project/blockchain/config/settings/settings.component';

const routes: Routes = [
  { path: '', redirectTo: 'dev', pathMatch: 'full' },
  { path: 'dev', component: BlockchainViewerComponent },

  { path: 'storage', component: StoreDataComponent },


  // { path: 'blockchainLazy', loadChildren: './project/blockchain/blockchain.module#BlockChainModule'},
  { path: 'blockchain', component: BlockchainViewerComponent },
  { path: 'blockchain/settings', component: SettingsComponent },
  { path: 'blockchain/new/transaction', component: CreateTransactionComponent },
  { path: 'blockchain/new/transaction/pending', component: PendingTransactionsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
