import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreDataComponent } from './store-data/store-data.component';
import { StoreDataService } from './store-data/store-data.service';
import { CardStorageComponent } from './store-data/components/card-storage/card-storage.component';
import { ButtonComponent } from './store-data/components/button/button.component';
import { BlockchainModule } from './blockchain/blockchain.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    StoreDataComponent,
    CardStorageComponent,
    ButtonComponent,
  ],
  imports: [
    CommonModule,
    BlockchainModule,
    FormsModule
  ],
  exports: [
  ],
  providers: [
    StoreDataService,
  ]
})
export class ProjectModule { }
