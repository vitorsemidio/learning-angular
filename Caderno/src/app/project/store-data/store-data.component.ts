import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { StoreDataService } from './store-data.service';

@Component({
  selector: 'app-store-data',
  templateUrl: './store-data.component.html',
  styleUrls: ['./store-data.component.scss']
})
export class StoreDataComponent implements OnInit {
  @ViewChild('key') key: ElementRef;
  @ViewChild('value') value: ElementRef;

  storageEnum = ['Cookie', 'Local', 'Session'];

  constructor(
    private storeDataService: StoreDataService,
  ) {
   }

  ngOnInit() {

  }

}
