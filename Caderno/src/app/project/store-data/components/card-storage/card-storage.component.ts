import { Component, OnInit, Input } from '@angular/core';
import { StoreDataService } from '../../store-data.service';

@Component({
  selector: 'app-card-storage',
  templateUrl: './card-storage.component.html',
  styleUrls: ['./card-storage.component.scss']
})
export class CardStorageComponent implements OnInit {
  @Input() tipoStorage = 'Cookie';

  constructor(
    private storeDataService: StoreDataService
  ) { }

  ngOnInit() {
  }

  salvarCookie(chave, valor) {
    this.storeDataService.salvarCookie(chave, valor);
  }
  getCookie(chave: string) {
    console.log(`ChaveGet: ${chave} | Valor: ${this.storeDataService.getCookie(chave)}`);
    this.storeDataService.getCookie(chave);
  }
  apagarCookie(chave: string) {
    console.log(`ChaveRemove: ${chave} | Valor: ${this.storeDataService.getCookie(chave)}`);
    this.storeDataService.apagarCookie(chave);
  }

  salvarLocalST(chave: string, valor: string) {
    this.storeDataService.salvarLocalST(chave, valor);
  }
  getLocalST(chave: string) {
    console.log(`ChaveGetST: ${chave} | Valor: ${this.storeDataService.getLocalST(chave)}`);
    this.storeDataService.getLocalST(chave);
  }
  apagarLocalST(chave: string) {
    console.log(`ChaveRemoveST: ${chave} | Valor: ${this.storeDataService.getLocalST(chave)}`);
    this.storeDataService.apagarLocalST(chave);
  }


  salvarSessionSt(chave: string, valor: string) {
    this.storeDataService.salvarSessionSt(chave, valor);
  }
  getSessionSt(chave: string) {
    console.log(`ChaveGetSession: ${chave} | Valor: ${this.storeDataService.getSessionSt(chave)}`);
    this.storeDataService.getSessionSt(chave);
  }
  apagarSessionSt(chave: string) {
    console.log(`ChaveRemoveSession: ${chave} | Valor: ${this.storeDataService.getSessionSt(chave)}`);
    this.storeDataService.apagarSessionSt(chave);
  }

  limparTudo() {
    this.storeDataService.limparTudo();
  }

  limparCookie() {
    this.storeDataService.limparCookie();
  }

  limparLocalStorage() {
    this.storeDataService.limparLocalStorage();
  }

  limparSessionStorage() {
    this.storeDataService.limparSessionStorage();
  }

}
