import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
  @Input() key: string;
  @Input() value: string;

  constructor() { }

  ngOnInit() {
    console.log('Chave inicial', this.key)
  }

  salvarCookie(chave, valor) {
    console.log('salvarCookie')
    // this.storeDataService.salvarCookie(chave, valor);
  }
  getCookie(chave: string) {
    console.log('getCookie')
    // this.storeDataService.getCookie(chave);
    // console.log(`ChaveGet: ${chave} | Valor: ${this.storeDataService.getCookie(chave)}`);
  }
  apagarCookie(chave: string) {
    console.log('apagarCookie')
    // this.storeDataService.apagarCookie(chave);
    // console.log(`ChaveRemove: ${chave} | Valor: ${this.storeDataService.getCookie(chave)}`);
  }

  teste() {
    console.log(this.key);
  }

}
