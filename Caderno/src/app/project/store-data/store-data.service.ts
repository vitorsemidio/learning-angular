import { Injectable } from '@angular/core';
import { CookieService } from 'angular2-cookie/core';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';

@Injectable({
  providedIn: 'root'
})
export class StoreDataService {

  constructor(
    private cookieService: CookieService,
    private localStorageService: LocalStorageService,
    private sessionSt: SessionStorageService,
  ) { }

  limparTudo() {
    this.limparCookie();
    this.limparLocalStorage();
    this.limparSessionStorage();
  }

  limparCookie() {
    this.cookieService.removeAll();
  }

  limparLocalStorage() {
    this.localStorageService.clear();
  }

  limparSessionStorage() {
    this.sessionSt.clear();
  }

  salvarCookie(chave: string, valor: string): void {

    this.cookieService.put(chave, valor);
  }
  getCookie(chave: string) {
    return this.cookieService.get(chave);
  }
  apagarCookie(chave: string) {
    this.cookieService.remove(chave);
  }


  salvarLocalST(chave: string, valor: string): void {

    this.localStorageService.store(chave, valor);
  }
  getLocalST(chave: string) {
    return this.localStorageService.retrieve(chave);
  }
  apagarLocalST(chave: string) {
    this.localStorageService.clear(chave);
  }


  salvarSessionSt(chave: string, valor: string): void {

    this.sessionSt.store(chave, valor);
  }
  getSessionSt(chave: string) {
    return this.sessionSt.retrieve(chave);
  }
  apagarSessionSt(chave: string) {
    this.sessionSt.clear(chave);
  }
}
