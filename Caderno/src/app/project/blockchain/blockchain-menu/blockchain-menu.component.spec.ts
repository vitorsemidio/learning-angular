import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlockchainMenuComponent } from './blockchain-menu.component';

describe('BlockchainMenuComponent', () => {
  let component: BlockchainMenuComponent;
  let fixture: ComponentFixture<BlockchainMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlockchainMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockchainMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
