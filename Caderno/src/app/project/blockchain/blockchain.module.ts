import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlockchainRoutingModule } from './blockchain-routing.module';
import { BlockchainMenuComponent } from './blockchain-menu/blockchain-menu.component';
import { BlockViewComponent } from './components/block-view/block-view.component';
import { TransactionsTableComponent } from './components/transactions-table/transactions-table.component';
import { BlockchainViewerComponent } from './pages/blockchain-viewer/blockchain-viewer.component';
import { PendingTransactionsComponent } from './pages/pending-transactions/pending-transactions.component';
import { CreateTransactionComponent } from './pages/create-transaction/create-transaction.component';
import { SettingsComponent } from './config/settings/settings.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    BlockchainMenuComponent,
    BlockViewComponent,
    TransactionsTableComponent,
    BlockchainViewerComponent,
    PendingTransactionsComponent,
    CreateTransactionComponent,
    SettingsComponent
  ],
  imports: [
    CommonModule,
    BlockchainRoutingModule,
    FormsModule
  ],
  exports: [
  ]
})
export class BlockchainModule { }
