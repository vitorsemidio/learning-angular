// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  webApiBaseUrl: 'http://localhost:62187/api/',
  servicoImagensApiBaseUrl: 'http://desenvolvimento.tce.rj.gov.br/servico-imagem-servidor/api/',
  auth: {
    authorityUrl: 'http://desenvolvimentodotcore.tce.rj.gov.br/controle-autenticacao/',
    client_id: '<myClienteID_REPLACE_IT>',
    redirect_uri: 'http://localhost:4200/auth.html',
    silent_redirect_uri: 'http://localhost:4200/silent-renew.html',
    post_logout_redirect_uri: 'http://localhost:4200/login',
    scope: 'openid profile tce.profile <myAppID_REPLACE_IT>'
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
