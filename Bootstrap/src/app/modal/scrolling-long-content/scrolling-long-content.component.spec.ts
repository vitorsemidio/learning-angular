import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScrollingLongContentComponent } from './scrolling-long-content.component';

describe('ScrollingLongContentComponent', () => {
  let component: ScrollingLongContentComponent;
  let fixture: ComponentFixture<ScrollingLongContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScrollingLongContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScrollingLongContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
