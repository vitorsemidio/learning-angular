import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModalRoutingModule } from './modal-routing.module';
import { LiveDemoComponent } from './live-demo/live-demo.component';
import { ScrollingLongContentComponent } from './scrolling-long-content/scrolling-long-content.component';

@NgModule({
  declarations: [LiveDemoComponent, ScrollingLongContentComponent],
  imports: [
    CommonModule,
    ModalRoutingModule
  ]
})
export class ModalModule { }
