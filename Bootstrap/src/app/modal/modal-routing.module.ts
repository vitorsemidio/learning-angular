import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LiveDemoComponent } from './live-demo/live-demo.component';

// const routes: Routes = [];

const routes: Routes = [
  {path: '', component: LiveDemoComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModalRoutingModule { }
