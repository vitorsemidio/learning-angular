import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutlineButtonsComponent } from './outline-buttons.component';

describe('OutlineButtonsComponent', () => {
  let component: OutlineButtonsComponent;
  let fixture: ComponentFixture<OutlineButtonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutlineButtonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutlineButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
