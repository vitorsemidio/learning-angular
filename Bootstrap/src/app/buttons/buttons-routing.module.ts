import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OutlineButtonsComponent } from './outline-buttons/outline-buttons.component';

const routes: Routes = [
  {
    path: '',
    component: OutlineButtonsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ButtonsRoutingModule { }
