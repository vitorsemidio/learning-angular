import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ButtonsRoutingModule } from './buttons-routing.module';
import { OutlineButtonsComponent } from './outline-buttons/outline-buttons.component';

@NgModule({
  declarations: [OutlineButtonsComponent],
  imports: [
    CommonModule,
    ButtonsRoutingModule
  ]
})
export class ButtonsModule { }
