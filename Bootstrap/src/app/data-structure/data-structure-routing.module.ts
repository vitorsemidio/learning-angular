import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DataStructureComponent } from './data-structure.component';
import { FilaComponent } from './fila/fila.component';
import { PilhaComponent } from './pilha/pilha.component';
import { DataComponent } from './data/data.component';

const routes: Routes = [
  {
    path: '', component: DataStructureComponent,
      children: [
        { path: 'fila', component: FilaComponent },
        { path: 'pilha', component: PilhaComponent },
        { path: 'data', component: DataComponent },
      ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DataStructureRoutingModule { }
