import { Component, OnInit } from '@angular/core';

export interface Pessoa {
  nome: string;
  idade: number;
  veiculo: Automovel;
}

export interface Automovel {
  placa: string;
  modelo: string;
}

export interface Vaga {
  numero: number;
  automovel?: Automovel;
  hora_entrada?: Date;
}

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.scss']
})
export class DataComponent implements OnInit {

  automoveis: Array<Automovel> = [
    { placa: 'aaa-2000', modelo: 'XYZ' },
    { placa: 'bbb-2000', modelo: 'ABC' },
    { placa: 'ccc-2000', modelo: 'IJK' },
  ];

  pessoas: Array<Pessoa> = [
    { nome: 'Emidio', idade: 20, veiculo: this.automoveis[0] },
    { nome: 'Luisa', idade: 21, veiculo: this.automoveis[1] },
    { nome: 'Rafael', idade: 22, veiculo: this.automoveis[2] },
  ];

  vagas: Array<Vaga> = [
    { numero: 1 },
    { numero: 2, automovel: this.automoveis[0] },
    { numero: 3 },
  ];

  constructor() { }

  ngOnInit() {
  }

  ocuparVaga(vaga: Vaga, pessoa?: Pessoa) {
    const numeroVaga = vaga.numero;
    console.log('Vaga numero', numeroVaga);
    console.log(vaga.automovel);
    if (vaga.automovel === undefined) {
      console.log('Vaga disponivel');
      this.setVaga(vaga, pessoa.veiculo);
      // console.log(pessoa.veiculo);
    } else {
      console.log('Vaga ocupada');
    }
  }

  setVaga(vaga: Vaga, automovel: Automovel) {
    vaga.automovel = automovel;
  }

}
