import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DataStructureRoutingModule } from './data-structure-routing.module';
import { DataStructureComponent } from './data-structure.component';
import { FilaComponent } from './fila/fila.component';
import { PilhaComponent } from './pilha/pilha.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DataComponent } from './data/data.component';

@NgModule({
  declarations: [DataStructureComponent, FilaComponent, PilhaComponent, DataComponent],
  imports: [
    CommonModule,
    DataStructureRoutingModule,
    ReactiveFormsModule
  ]
})
export class DataStructureModule { }
