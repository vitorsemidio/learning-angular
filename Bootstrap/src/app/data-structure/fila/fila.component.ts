import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

export interface Pessoa {
  nome: string;
  idade: number;
  veiculo: Automovel;
}

export interface Automovel {
  placa: string;
  modelo: string;
}

export interface Vaga {
  automovel: Automovel;
  numero: number;
  hora_entrada: Date;
}


@Component({
  selector: 'app-fila',
  templateUrl: './fila.component.html',
  styleUrls: ['./fila.component.scss']
})
export class FilaComponent implements OnInit {
  @Input() dados;
  fila: Array<any> = new Array();
  @ViewChild('inputTag') numero: ElementRef;
  name = new FormControl('');
  profileForm = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
  });

  automoveis: Array<Automovel> = [
    { placa: 'aaa-2000', modelo: 'XYZ' },
    { placa: 'bbb-2000', modelo: 'ABC' },
    { placa: 'ccc-2000', modelo: 'IJK' },
  ];

  pessoas: Array<Pessoa> = [
    { nome: 'Emidio', idade: 20, veiculo: this.automoveis[0] },
    { nome: 'Luisa', idade: 21, veiculo: this.automoveis[1] },
    { nome: 'Rafael', idade: 22, veiculo: this.automoveis[2] },
  ];

  constructor() { }

  ngOnInit() {

  }

  enfileira(elemento: number): void {
    this.fila.push( elemento );
  }

  desenfileira() {
    this.fila.shift();
  }


  imprimir( dado: ElementRef ) {
    console.log( dado.nativeElement.valueAsNumber );
  }

  // Formulario
  updateValue( newData ) {
    this.name.setValue(newData);
  }

  ocuparVaga( dado ) {
    this.fila.push(dado);
  }

  desocuparVaga() {
    this.fila.shift();
  }

  onSubmit() {
    console.warn(this.profileForm.value);
  }

}
