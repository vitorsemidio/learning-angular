import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LayoutModule } from './layout/layout.module';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AppRoutingModule } from './app.routing.module';
import { SharedModule } from './shared/shared.module';
import { ApiService } from './services/api.service';
import { AuthService } from './services/auth.service';
import { ImagensService } from './services/imagens.service';
import { ResponseHandlerService } from './services/response-handler.service';
import { AuthorizationGuard } from './guards/authorizationGuard';
import { AlertsComponent } from './alerts/alerts.component';
import { NavbarComponent } from './navbar/navbar.component';
import { CardComponent } from './card/card.component';
import { StyleModule } from './style/style.module';
import { DataStructureModule } from './data-structure/data-structure.module';
import { FormsModule } from '@angular/forms';
import { FormularioModule } from './formulario/formulario.module';
import { RxjsTutorialComponent } from './pages/rxjs-tutorial/rxjs-tutorial.component';
import { RxjsTutorialService } from './pages/rxjs-tutorial/rxjs-tutorial.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NotFoundComponent,
    AlertsComponent,
    NavbarComponent,
    CardComponent,
    RxjsTutorialComponent,
  ],
  imports: [
    BrowserModule,
    LayoutModule,
    AppRoutingModule,
    SharedModule,
    StyleModule,
    DataStructureModule,
    FormsModule,
    FormularioModule
  ],
  providers: [
    ApiService,
    AuthService,
    AuthorizationGuard,
    ImagensService,
    ResponseHandlerService,
    RxjsTutorialService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
