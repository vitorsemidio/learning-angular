import { LogicaComponent } from './logica.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: LogicaComponent },
  { path: '', children: [
    // { path: 'card', component: CardComponent },
    // { path: 'checkbox', component: CheckboxComponent },
    // { path: 'box', component: BoxComponent },
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LogicaRoutingModule { }
