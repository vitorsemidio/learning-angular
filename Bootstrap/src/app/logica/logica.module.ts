import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LogicaRoutingModule } from './logica-routing.module';
import { LogicaComponent } from './logica.component';

@NgModule({
  declarations: [LogicaComponent],
  imports: [
    CommonModule,
    LogicaRoutingModule
  ]
})
export class LogicaModule { }
