import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-logica',
  templateUrl: './logica.component.html',
  styleUrls: ['./logica.component.scss']
})
export class LogicaComponent implements OnInit {
  p = [];
  q = [];
  x = [][4];
  vartmp = ['E', 'OU', 'SSS', 'IMPL', 'XOU'];
  arr: Array<any>;

  proposicoes: boolean;

  constructor() { }

  ngOnInit() {
    this.gerarPreposicoes(2);
    // this.arr = this.createArray(2);
    // console.log(this.arr.length);
    this.arr = this.createMatriz(3);
    console.log(this.arr);
  }
  operacao(a, b, op) {
    switch (op) {
      case 'E':
        return a && b;
      case 'OU':
        return a || b;
      case 'SSS':
        return a === b;
      case 'IMPL':
        return !a || b;
      case 'XOU':
        return a !== b;
      default:
        return null;
    }
  }

  NOT( a ) {
    return !a;
  }

  gerarPreposicoes( qtd: number ) {
    const limite = Math.pow( qtd, 2 );
    this.p = []; this.q = [];
    for ( let i = 0; i < limite; i++ ) {
      if (i < limite / 2) {
        this.p.push(true);
      } else {
        this.p.push(false);
      }
      if ( i % 2 === 0 ) {
        this.q.push(true);
      } else {
        this.q.push(false);
      }
    }

    console.log(this.p);
    console.log(this.q);
  }

  execucao(operacao: string) {
    if (this.p && this.q) {
      for (let i = 0; i < this.p.length; i++) {
        console.log(this.p[i], this.q[i], this.operacao(this.p[i], this.q[i], operacao));
      }
    }
  }

  teste() {
    const p = this.p;
    const q = this.q;
    if (p && q) {
      for (let i = 0; i < p.length; i++) {
        console.log(this.operacao(this.operacao(p[i], q[i], 'OU'), p[i], 'E'));
      }
    }
  }

  funcaoDeclarativa( a, fn, b ) {
    return fn( a, b );
  }

  E(a,b) {
    return a && b;
  }

  funcaoDeclarativaTeste() {
    for (let i = 0; i < 4; i++) {
      console.log( this.funcaoDeclarativa( this.p[i], this.E, this.q[i] ));
    }
  }

  createArray(length) {
    const arr = new Array(length || 0);
    let i = length;

    if (arguments.length > 1) {
        var args = Array.prototype.slice.call(arguments, 1);
        while(i--) arr[length-1 - i] = this.createArray.apply(this, args);
    }

    return arr;
  }

  createMatriz(x) {
    const matriz = new Array(x);
    const lin = Math.pow(2, x);
    for (let i = 0; i < matriz.length; i++) {
      matriz[i] = new Array(lin);
    }
    // console.log(matriz);
    // return matriz;


    // Popula
    for (let i = 0; i < matriz.length; i++) {
      const limiteTrue =  Math.pow(2, x - i);
      for (let j = 0; j < lin; j++) {
        matriz[i][j] = ( j % limiteTrue ) < ( limiteTrue / 2 );
      }
      // console.log(i, matriz[i]);
    }
    // console.log(matriz);
    return matriz;
  }



}
