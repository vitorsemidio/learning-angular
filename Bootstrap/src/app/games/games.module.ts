import { DoiskService } from './doisk/doisk.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GamesRoutingModule } from './games-routing.module';
import { SnakeComponent } from './snake/snake.component';
import { DoiskComponent } from './doisk/doisk.component';
import { HomeComponent } from './home/home.component';
import { ControleDirective } from './doisk/controle.directive';
import { TutorialJsComponent } from './tutorial-js/tutorial-js.component';

@NgModule({
  declarations: [SnakeComponent, DoiskComponent, HomeComponent, ControleDirective, TutorialJsComponent],
  imports: [
    CommonModule,
    GamesRoutingModule
  ],
  providers: [DoiskService]
})
export class GamesModule { }
