import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SnakeComponent } from './snake/snake.component';
import { DoiskComponent } from './doisk/doisk.component';
import { TutorialJsComponent } from './tutorial-js/tutorial-js.component';

export const routes: Routes = [
  {
    path: '', component: HomeComponent,
    children: [
      { path: 'snake', component: SnakeComponent },
      { path: '2048', component: DoiskComponent },
      { path: 'tutorial-js', component: TutorialJsComponent },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GamesRoutingModule { }
