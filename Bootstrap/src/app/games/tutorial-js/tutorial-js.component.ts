import { Component, OnInit } from '@angular/core';

export class Metodo {
  nome: string;
  descricao: string;
}

@Component({
  selector: 'app-tutorial-js',
  templateUrl: './tutorial-js.component.html',
  styleUrls: ['./tutorial-js.component.scss']
})
export class TutorialJsComponent implements OnInit {
  // api: speech

  arrayMethods: Metodo[] = [
    {nome: 'splice', descricao: 'Adds and/or removes elements from an array.'},
    {nome: 'fill', descricao: 'Fills all the elmenets of an array from a start index to an end index with a static value'},
    {nome: 'concat', descricao: 'Returns a new array comprised of this array joined with other array(s) and/or value(s)'}
  ];

  array = [];

  constructor() { }

  ngOnInit() {
  }



}
