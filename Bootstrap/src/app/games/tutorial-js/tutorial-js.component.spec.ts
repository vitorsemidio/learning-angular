import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TutorialJsComponent } from './tutorial-js.component';

describe('TutorialJsComponent', () => {
  let component: TutorialJsComponent;
  let fixture: ComponentFixture<TutorialJsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TutorialJsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TutorialJsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
