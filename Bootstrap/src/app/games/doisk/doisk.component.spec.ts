import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoiskComponent } from './doisk.component';

describe('DoiskComponent', () => {
  let component: DoiskComponent;
  let fixture: ComponentFixture<DoiskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoiskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoiskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
