import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DoiskService {
  altura = 4;
  largura = 4;

  constructor() { }

  left(tabela) {
    let score = 0;
    for (let linha = 0; linha < this.altura; linha++) {
      for (let coluna = this.largura-1; coluna >=0; coluna--) {
        const index = this.altura * linha + coluna;
        if (index % this.largura != 0) {
          if (tabela[index - 1] == 0 || tabela[index] == tabela[index - 1]) {
            if (tabela[index] == tabela[index - 1]) {
              score += tabela[index]*2;
            }
            tabela[index - 1] += tabela[index];
            tabela[index] = 0;
          }
        }
      }
    }
    this.gerarNovoValor(tabela);
    return score;
  }

  right(tabela) {
    let score = 0;
    for (let linha = 0; linha < this.altura; linha++) {
      for (let coluna = 0; coluna < this.largura; coluna++) {
        const index = this.altura * linha + coluna;
        if (index % this.largura != 3) {
          if (tabela[index + 1] == 0 || tabela[index] == tabela[index + 1]) {
            if (tabela[index] == tabela[index + 1]) {
              score += tabela[index]*2;
            }
            tabela[index + 1] += tabela[index];
            tabela[index] = 0;
          }
        }
      }
    }
    this.gerarNovoValor(tabela);
    return score;
  }

  up(tabela) {
    let score = 0;
    for (let linha = this.altura-1; linha >= 0; linha--) {
      for (let coluna = this.largura-1; coluna >= 0; coluna--) {
        const index = this.altura * linha + coluna;
        // console.log(index);
        if (tabela[index - this.largura] == 0 || tabela[index] == tabela[index - this.largura]) {
          if (tabela[index] == tabela[index - this.largura]) {
            score += tabela[index]*2;
          }
          tabela[index - this.largura] += tabela[index];
          tabela[index] = 0;
        }
      }
    }
    this.gerarNovoValor(tabela);
    return score;
  }

  down(tabela) {
    let score = 0;
    for (let linha = 0; linha < this.altura; linha++) {
      for (let coluna = 0; coluna < this.largura; coluna++) {
        const index = this.altura * linha + coluna;
        // console.log(index);
        if (tabela[index + this.largura] == 0 || tabela[index] == tabela[index + this.largura]) {
          if (tabela[index] == tabela[index + this.largura]) {
            score += tabela[index]*2;
          }
          tabela[index + this.largura] += tabela[index];
          tabela[index] = 0;
        }
      }
    }
    this.gerarNovoValor(tabela);
    return score;
  }

  gerarNovoValor(tabela) {
    let tentativas = 0;
    const espacoAmostral = [2, 4];
    const aleatorio = Math.floor(Math.random() * 2);
    let posicao = this.gerarPosicao();
    while(tabela[posicao] != 0) {
      posicao = this.gerarPosicao();
      tentativas++;
      if (tentativas >= 5) {
        let x = -1;
        for (let i = 0; i < this.largura * this.altura -1; i++) {
          console.log('posicao', i);
          if (tabela[i] == 0) {
            x = i;
            break;
          }
        }
        if (x != -1) {
          posicao = x;
        } else {
          alert('Game Over');
          break;
        }
      }
    }

    tabela[posicao] = espacoAmostral[aleatorio];
  }

  gerarPosicao() {
    return Math.floor(Math.random() * this.largura * this.altura);
  }
}
