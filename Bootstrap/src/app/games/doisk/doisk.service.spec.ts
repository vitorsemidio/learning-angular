import { TestBed } from '@angular/core/testing';

import { DoiskService } from './doisk.service';

describe('DoiskService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DoiskService = TestBed.get(DoiskService);
    expect(service).toBeTruthy();
  });
});
