import { DoiskService } from './doisk.service';
import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-doisk',
  templateUrl: './doisk.component.html',
  styleUrls: ['./doisk.component.scss']
})
export class DoiskComponent implements OnInit {
  key;
  score = 0;
  largura = 4;
  altura = 4;
  tabela = [
    0, 2, 4, 4,
    2, 0, 0, 0,
    4, 2, 2, 0,
    0, 2, 0, 2
  ];
  zero = 0;


  constructor(
    private controle: DoiskService
  ) { }

  ngOnInit() {
    this.reiniciar(3);
  }


  cima() {
    this.score += this.controle.up(this.tabela);
  }
  esquerda() {
    this.score += this.controle.left(this.tabela);
  }
  baixo() {
    this.score += this.controle.down(this.tabela);
  }
  direita() {
    this.score += this.controle.right(this.tabela);
  }

  resetarScore() {
    this.score = 0;
  }

  reiniciar(index: number) {
    const exemplo = [
    [
      0, 2, 4, 4,
      0, 0, 4, 0,
      0, 2, 4, 0,
      0, 2, 4, 2
    ],
    [
      0, 2, 4, 4,
      0, 0, 4, 0,
      0, 2, 4, 0,
      0, 2, 4, 4
    ],
    [
      0, 2, 4, 4,
      4, 4, 4, 4,
      0, 2, 4, 0,
      0, 2, 4, 4
    ],
    [
      0, 0, 0, 0,
      0, 0, 0, 4,
      0, 2, 0, 0,
      0, 0, 0, 2
    ],
  ];
    this.tabela = exemplo[index];
    this.resetarScore();
  }



  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    this.key = event.keyCode;
    switch (this.key) {
      case 37: // Left
        return this.esquerda();
      case 38: // up
        return this.cima();
      case 39: // right
        return this.direita();
      case 40: // down
        return this.baixo();
      default:
        return 0;
    }
  }


}
