import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appControle]'
})
export class ControleDirective {

  constructor(element: ElementRef) {
    element.nativeElement.style.color = 'blue';
   }

   @HostListener('click') faca() {
     console.log('click');
    //  alert('funcionou');
   }

   @HostListener('keyup') saida() {
     console.log('tecla precionada');
   }

}
