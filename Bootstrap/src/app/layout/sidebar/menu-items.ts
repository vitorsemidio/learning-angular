import { MenuItemModel } from './menu-item.model';

export const MENUITEMS: Array<MenuItemModel> = [
  { title: 'Painel de controle', icon: 'icons icon-speedometer', router: '/home', items: [] },
  { title: 'Alerts', icon: 'icons icon-user', router: '/alerts', items: [] },
  { title: 'Navbar', icon: 'icons icon-social-github', router: '/navbar', items: [] },
  { title: 'Modal', icon: 'icons icon-direction', router: '/modal', items: [] },
  { title: 'Collapse', icon: 'icons icon-map', router: '/collapse', items: [] },
  { title: 'Buttons', icon: 'icons icon-phone', router: '/buttons', items: [] },
  { title: 'Cards', icon: 'icons icon-credit-card', router: '/card', items: [] },
  { title: 'Elementos de Logica', icon: 'icons icon-social-facebook', router: '/logica', items: [] },
  { title: 'Games', icon: 'icons icon-credit-card', router: '/games', items: [] },
  { title: 'Online Tutorial', icon: 'icons icon-fire', router: '/tutorial-css', items: [] },
  { title: 'Estrutura de Dados', icon: 'icons icon-globe', router: '/data-structure', items: [] },
];
