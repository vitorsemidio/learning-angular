import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormularioRoutingModule } from './formulario-routing.module';
import { ReactFormComponent } from './react-form/react-form.component';
import { TemplateDrivenComponent } from './template-driven/template-driven.component';
import { FormularioComponent } from './formulario.component';

@NgModule({
  declarations: [ReactFormComponent, TemplateDrivenComponent, FormularioComponent],
  imports: [
    CommonModule,
    FormularioRoutingModule
  ]
})
export class FormularioModule { }
