import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';

import { AlertsComponent } from './alerts/alerts.component';
import { NavbarComponent } from './navbar/navbar.component';
import { CardComponent } from './card/card.component';
import { FormularioComponent } from './formulario/formulario.component';
import { RxjsTutorialComponent } from './pages/rxjs-tutorial/rxjs-tutorial.component';

const routes: Routes = [
    { path: '', redirectTo: 'developing', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    { path: 'developing', component: RxjsTutorialComponent },

    // remover dps para usar lazy loading
    { path: 'alerts', component: AlertsComponent },
    { path: 'navbar', component: NavbarComponent },
    { path: 'card', component: CardComponent },

    { path: 'customers', loadChildren: './customers/customers.module#CustomersModule' },
    { path: 'orders', loadChildren: './orders/orders.module#OrdersModule' },
    { path: 'buttons', loadChildren: './buttons/buttons.module#ButtonsModule' },
    { path: 'modal', loadChildren: './modal/modal.module#ModalModule' },
    { path: 'collapse', loadChildren: './collapse/collapse.module#CollapseModule'},
    { path: 'games', loadChildren: './games/games.module#GamesModule'},
    { path: 'logica', loadChildren: './logica/logica.module#LogicaModule'},
    { path: 'tutorial-css', loadChildren: './style/style.module#StyleModule'},
    { path: 'data-structure', loadChildren: './data-structure/data-structure.module#DataStructureModule'},
    // { path: 'card', loadChildren: './card/card.module#CardModule'},

    // not found
    { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
