import { Injectable } from '@angular/core';
import { Observable, Subject, BehaviorSubject, ReplaySubject, AsyncSubject } from 'rxjs';
import { of, from, interval, range, generate, fromEvent, defer, bindCallback, bindNodeCallback } from 'rxjs';
import { share, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RxjsTutorialService {
  pessoas = [
    { nome: 'Agnes', sexo: 'feminino', idade: 23 },
    { nome: 'Emidio', sexo: 'masculino', idade: 20 },
    { nome: 'Julia', sexo: 'feminino', idade: 22 },
    { nome: 'Luiza', sexo: 'feminino', idade: 17 },
    { nome: 'Rafael', sexo: 'masculino', idade: 21 },
  ];

  constructor() { }


  getPessoas() {
    return this.pessoas;
  }

  exemploDev() {

    const sub = new Subject();

    const observable = Observable.create((observer) => {
      observer.next(1);
      observer.next(2);
      observer.next(3);
      observer.next(4);
      observer.next(5);
    });


    const subscription2 = sub.subscribe(
      num => console.log('subscription 2', num),
      err => console.log('erro', err),
      () => console.log('completado')
    );
    // teve que ficar em baixo pq emite valor independente de ter alguem ouvindo.
    // logo, se a subscricao acontecesse antes, ja ocorreria as emissoes e o subscription2 ouviria nada
    const subscription1 = observable.subscribe(sub);

    subscription1.add(subscription2); // remove para remover
    subscription1.unsubscribe();

  }

  exemploOperadores() {
    const button = document.querySelector('button');
    function* g() {
      let i = 1;
      while(true) {
          yield i++;
      }
  }
    const funOf = () => {
      of(1, true, 'string', [1,2,3]).subscribe(x => console.log(x))
    }

    const funFrom = () => {
        from([1,2,3]).subscribe(x => console.log(x))
        from(Promise.resolve('kdsk  )')).subscribe(x => console.log(x))
        from(g())
            .pipe(
                take(10)
            )
            .subscribe(x => console.log(x))
    }


    const funInterval = () => {
        interval(1000)
            .pipe(take(10))
            .subscribe(v => console.log(v));
    }


    const funRange = () => {
        range(500, 20)
            .subscribe(v => console.log(v));
    }


    const funGenerate = () => {
        generate( 0, x => x < 10, x => x+1 )
            .subscribe(v => console.log(v))
    }

    const funFromEvent = () => {
        fromEvent(button, 'click')
            .subscribe(v => console.log(v))
    }


    // encapsula uma funcao e retornar um observable
    const funDefer = (a) => defer(() => {
        return a > 10 ? of(1) : of(2);
    })

    const exeDefer = () => {
        funDefer(5).subscribe( v => console.log(v) )
        funDefer(15).subscribe( v => console.log(v) )
    }
    // exeDefer();

    const funBindCallBack = () => {
        const a = (a, callback) => {
            callback(a);
        }

        bindCallback(a)(10)
            .subscribe( v => console.log(v) )

    }

    const funBindNodeCallback = () => {
        const b1 = (b, callback) => {
            callback(new Error('erro'), b);
        }
        const b2 = (b, callback) => {
            callback(undefined, b);
        }

        bindNodeCallback(b1)(10)
            .subscribe(
                v => console.log(v),
                err => console.log(err)
        )
        bindNodeCallback(b2)(10)
            .subscribe(
                v => console.log(v),
                err => console.log(err)
        )

    }

    funOf()
    funFrom()
    funInterval()
    funRange()
    funGenerate()
    funFromEvent()
    exeDefer()
    funBindCallBack();
    funBindNodeCallback();
  }

  exemplo13() {
    const sub = new AsyncSubject();
    const objObserver = {
        next: num => console.log('Replay Subject', num),
        error: err => console.log('Replay Subject', err),
        complete: () => console.log('Completado Replay Subject')
    }

    sub.next(1);
    sub.next(2);
    sub.next(3);
    sub.next(4);

    sub.subscribe(objObserver)

    sub.complete();
  }

  async exemplo12() {
    // Quantos valores emitidos serão propagados para quem se inscrever
    const sub = new ReplaySubject(3);
    const subComTempoDeCache = new ReplaySubject(2, 300);
    const objObserver = {
        next: num => console.log('Replay Subject', num),
        error: err => console.log('Replay Subject', err),
        complete: () => console.log('Completado Replay Subject')
    }

    sub.next(1);
    sub.next(2);
    sub.next(3);
    sub.next(4);

    sub.subscribe(objObserver)

    setTimeout(() => subComTempoDeCache.next(5), 100);
    setTimeout(() => subComTempoDeCache.next(6), 200);
    setTimeout(() => subComTempoDeCache.next(7), 300);
    setTimeout(() => subComTempoDeCache.next(8), 1400);

    setTimeout(() => {
        subComTempoDeCache.subscribe(objObserver)
        console.log('fim');
    }, 600)
  }

  async exemplo11() {
    const subBehavior = new BehaviorSubject(0); // mesma coisa que um ReplaySubject(1)

    const subscription1 = subBehavior.subscribe(
        num => console.log('Behavior 1', num),
        erro => console.log('erro', erro),
        () => console.log('complete')
    )

    subBehavior.next(1);
    subBehavior.next(2);

    const subscription2 = subBehavior.subscribe(
        num => console.log('Behavior 2', num),
        erro => console.log('erro', erro),
        () => console.log('complete')
    )

    subBehavior.next(3);

    const subscription3 = subBehavior.subscribe(
        num => console.log('Behavior 3', num),
        erro => console.log('erro', erro),
        () => console.log('complete')
    )

    subBehavior.next(4);
  }


  exemplo10() {

    const sub = new Subject();

    const observable = Observable.create((observer) => {
      observer.next(1);
      observer.next(2);
      observer.next(3);
      observer.next(4);
      observer.next(5);
    });


    const subscription2 = sub.subscribe(
      num => console.log('subscription 2', num),
      err => console.log('erro', err),
      () => console.log('completado')
    );
    // teve que ficar em baixo pq emite valor independente de ter alguem ouvindo.
    // logo, se a subscricao acontecesse antes, ja ocorreria as emissoes e o subscription2 ouviria nada
    const subscription1 = observable.subscribe(sub);

    subscription1.add(subscription2); // remove para remover
    subscription1.unsubscribe();

  }


  exemplo9() {
    const promise = new Promise((resolve) => {
      resolve(1);
    });

    const observer1 = Observable.create((observer) => {
      let i = 0;
      console.log('iniciando observable');
      const interval = setInterval(() => observer.next(i++), 1100);
      setTimeout(() => observer.complete(), 15000);

      return () => clearInterval(interval);
    });
    const observer2 = Observable.create((observer) => {
      let i = 0;
      const interval = setInterval(() => observer.next(i++), 1500);
      setTimeout(() => observer.complete(), 12500);

      return () => clearInterval(interval);
    });

    const subscription1 = observer1.subscribe(
      num => console.log('subscription 1', num),
      err => console.log('erro', err),
      () => console.log('completado')
    )
    const subscription2 = observer1.subscribe(
      num => console.log('subscription 2', num),
      err => console.log('erro', err),
      () => console.log('completado')
    )


    // Ao se desinscrever de subscription 1, automaticamente ira se desinscrever do 2.
    subscription1.add(subscription2); // remove para remover



    setTimeout(() => {
      subscription1.unsubscribe();
    }, 11111);
  }

  exemplo8() {
    const promise = new Promise((resolve) => {
      resolve(1);
    });

    const observer$ = Observable.create((observer) => {
      let i = 0;
      console.log('iniciando observable');
      const interval = setInterval(() => observer.next(i++), 1500);
      setTimeout(() => observer.complete(), 10000);

      return () => clearInterval(interval);
    });

    const subscription = observer$.subscribe(
      num => console.log('Observable', num),
      err => console.log('erro', err),
      () => console.log('completado')
    )

    setTimeout(() => {
      // ao se desinscrever, irá parar de ouvir os eventos emitidos pelo stream.
      subscription.unsubscribe();
    }, 6000);
  }


  exemplo1() {
    // output: start promise; start observer; observer; promise
    new Promise((resolve) => {
      resolve(1);
    }).then(dado => console.log('Promise', dado));

    Observable.create((observer) => {
      observer.next(1);
    }).subscribe(dado => console.log('Observable', dado));
  }

  // Promise e observable resolvendo e emitindo 2x
  exemplo2() {
    // promise emite um unico valor
    new Promise((resolve) => {
      resolve(1);
      resolve(2);
    }).then(dado => console.log('Promise', dado));

    // observable emit multiplos valores
    Observable.create((observer) => {
      observer.next(1);
      observer.next(2);
    }).subscribe(dado => console.log('Observable', dado));
  }


  // Comportamento eager e lazy
  exemplo3() {
    // output: 'iniciando a promise'
    new Promise((resolve) => {
      console.log('Iniciando a promise');
      resolve(1);
    });

    // nao tera saida pq ngm se inscreveu
    // o bloco de codigo soh eh executado quando alguem se subinscreve
    Observable.create((observer) => {
      console.log('Iniciando a observable');
      observer.next(1);
    });
  }


  exemplo4() {
    // output: 'iniciando a promise'
    new Promise((resolve) => {
      console.log('Iniciando a promise');
      resolve(1);
    });

    // soh serah executado depois de dois segundos
    const observer$ = Observable.create((observer) => {
      console.log('Iniciando a observable');
      observer.next(1);
    });

    setTimeout(() => {
      observer$.subscribe(valor => console.log('Observable', valor));
    }, 2000);
  }

  // multicast e unicast
  exemplo5() {
    const promise = new Promise((resolve) => {
      console.log('Iniciando a promise');
      setTimeout(() => resolve(1), 3000);
    });

    const observer$ = Observable.create((observer) => {
      console.log('Iniciando a observable');
      setTimeout(() => observer.next(1), 3000);
    });

    promise.then(num => console.log('Promise', num));
    observer$.subscribe(num => console.log('Observable', num));


    setTimeout(() => {
      promise.then(num => console.log('Promise a', num));
      observer$.subscribe(valor => console.log('Observable a', valor));
    }, 2000);
  }

  // estado compartilhado
  exemplo6() {
    const promise = new Promise((resolve) => {
      console.log('Iniciando a promise');
      setTimeout(() => resolve(1), 3000);
    });

    // soh serah executado depois de dois segundos
    const observer$ = Observable.create((observer) => {
      console.log('Iniciando a observable');
      setTimeout(() => observer.next(1), 3000);
    }).pipe(
      share()
    )

    promise.then(num => console.log('Promise', num));
    observer$.subscribe(num => console.log('Observable', num));


    setTimeout(() => {
      promise.then(num => console.log('Promise a', num));
      observer$.subscribe(valor => console.log('Observable a', valor));
    }, 2000);
  }


  // inscricao cancelada
  exemplo7() {
    const promise = new Promise((resolve) => {
      resolve(1);
    });

    const observer$ = Observable.create((observer) => {
      let i = 0;
      const interval = setInterval(() => observer.next(i++), 1500);

      return () => clearInterval(interval);
    });

    promise.then(num => console.log('Promise', num));
    const subscriber = observer$.subscribe(num => console.log('Observable', num));

    setTimeout(() => {
      subscriber.unsubscribe();
    }, 7000);
  }
}
