import { TestBed } from '@angular/core/testing';

import { RxjsTutorialService } from './rxjs-tutorial.service';

describe('RxjsTutorialService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RxjsTutorialService = TestBed.get(RxjsTutorialService);
    expect(service).toBeTruthy();
  });
});
