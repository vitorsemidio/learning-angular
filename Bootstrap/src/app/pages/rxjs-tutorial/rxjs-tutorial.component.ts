import { Component, OnInit } from '@angular/core';
import { Observable, from } from 'rxjs';
import { filter, reduce } from 'rxjs/operators';
import { RxjsTutorialService } from './rxjs-tutorial.service';


@Component({
  selector: 'app-rxjs-tutorial',
  templateUrl: './rxjs-tutorial.component.html',
  styleUrls: ['./rxjs-tutorial.component.scss']
})
export class RxjsTutorialComponent implements OnInit {
  pessoas;


  constructor(
    private rxjsService: RxjsTutorialService
  ) { }

  ngOnInit() {
    // this.rxjsService.exemploDev();
    this.rxjsService.exemploOperadores();
    // this.testeSincrono();
  }


  // nao funcionou
  async testeSincrono() {
    console.log('11');
    await this.rxjsService.exemplo11();
    console.log('12');
    await this.rxjsService.exemplo12();
    console.log('fim');
  }

  maioresPorGenero() {
// tslint:disable-next-line: no-shadowed-variable
    const maioresAgrupadosPorGenero = (pessoas: { nome: string; sexo: string; idade: number; }[]) => {
      pessoas
      .filter(e => e.idade >= 18)
      .reduce((a, b) => ({
          ...a,
          [b.sexo]: [...(a[b.sexo] || []), b]
      }), {});
    };

    const pessoas = [
        { nome: 'Agnes', sexo: 'feminino', idade: 23 },
        { nome: 'Julia', sexo: 'feminino', idade: 22 },
        { nome: 'Luiza', sexo: 'feminino', idade: 17 },
        { nome: 'Rafael', sexo: 'masculino', idade: 21 },
        { nome: 'Emidio', sexo: 'masculino', idade: 20 },
    ]

    console.log(pessoas);
    console.log(pessoas.filter(e => e.idade >= 18));
    console.log(
      pessoas
        .filter(e => e.idade >= 18)
        .reduce((a, b) => ({
          ...a,
          [b.sexo]: [...(a[b.sexo] || []), b]
        }), {})
      );
    console.log(maioresAgrupadosPorGenero(pessoas)); // undefined

  }

  maioresPorGenero2() {
    const maioresAgrupadosPorGenero = () => source$ => {
      source$.pipe(
          filter(e => e['idade'] >= 18),
          reduce((a, b) => ({
              ...a,
              [b['sexo']]: [...(a[b['sexo']] || []), b]
          }), {})
      )
    }

    const pessoas = [
      { nome: 'Agnes', sexo: 'feminino', idade: 23 },
      { nome: 'Julia', sexo: 'feminino', idade: 22 },
      { nome: 'Luiza', sexo: 'feminino', idade: 17 },
      { nome: 'Rafael', sexo: 'masculino', idade: 21 },
      { nome: 'Emidio', sexo: 'masculino', idade: 20 },
    ]

    // maioresAgrupadosPorGenero(pessoas);
    from(pessoas)
      .pipe(
        // maioresAgrupadosPorGenero(),
      )
      .subscribe(e => console.log(e), err => console.error(err))
  }

}
