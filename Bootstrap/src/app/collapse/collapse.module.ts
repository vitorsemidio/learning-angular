import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CollapseRoutingModule } from './collapse-routing.module';
import { MultipleTargetsComponent } from './multiple-targets/multiple-targets.component';

@NgModule({
  declarations: [MultipleTargetsComponent],
  imports: [
    CommonModule,
    CollapseRoutingModule
  ],
  // exports: [MultipleTargetsComponent]
})
export class CollapseModule { }
