import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultipleTargetsComponent } from './multiple-targets.component';

describe('MultipleTargetsComponent', () => {
  let component: MultipleTargetsComponent;
  let fixture: ComponentFixture<MultipleTargetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultipleTargetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultipleTargetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
