import { BoxComponent } from './box/box.component';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { CardComponent } from './card/card.component';
import { HomeStyleComponent } from './home-style/home-style.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: HomeStyleComponent },
  { path: '', children: [
    { path: 'card', component: CardComponent },
    { path: 'checkbox', component: CheckboxComponent },
    { path: 'box', component: BoxComponent },
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StyleRoutingModule { }
