import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeStyleComponent } from './home-style.component';

describe('HomeStyleComponent', () => {
  let component: HomeStyleComponent;
  let fixture: ComponentFixture<HomeStyleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeStyleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeStyleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
