import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StyleRoutingModule } from './style-routing.module';
import { CardComponent } from './card/card.component';
import { HomeStyleComponent } from './home-style/home-style.component';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { BoxComponent } from './box/box.component';

@NgModule({
  declarations: [CardComponent, HomeStyleComponent, CheckboxComponent, BoxComponent],
  imports: [
    CommonModule,
    StyleRoutingModule
  ]
})
export class StyleModule { }
