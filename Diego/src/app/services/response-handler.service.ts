import { Observable, throwError } from 'rxjs';

import { Injectable } from '@angular/core';
import { Response, ResponseContentType } from '@angular/http';

@Injectable()
export class ResponseHandlerService {

    constructor() { }

    handleSuccess(response: Response, responseType: ResponseContentType): any {
        switch (responseType) {
            case ResponseContentType.Json:
                return response.json().data;
            case ResponseContentType.Blob:
                return response.blob();
            default:
                return response.text();
        }
    }
    handleError(response: Response): Observable<any> {
      /*let error: any;

      // tslint:disable-next-line:triple-equals
      if (response.status == 401) {
          error = {
              unauthorized: true
          };
      } else if (response.status === 400 || response.status === 404) {
        const result = response.json();

        if (result && result.erros) {
          let errors = '';

          for (let i = 0; i < result.errors.length; i++) {
            errors += `<p>${result.erros[i]}</p>`;
          }

          error = {
            title: 'TCE-RJ / Validacao',
            message: errors
          };
        }*/
        let error: any;

        if (response.status == 401) {
            error = {
                unauthorized: true
            };
        } else if (response.status === 400 || response.status === 404) {
            const result = response.json();

            if (result && result.errors) {
                let erros = '';

                for (let i = 0; i < result.errors.length; i++) {
                    erros += `<p>${result.errors[i]}</p>`;
                }

                error = {
                    title: 'TCE-RJ / Validação',
                    message: erros
                };
            }
        } else {
            error = this.getDefaultErrorObject();
        }

        return throwError(error);
    }

    private getDefaultErrorObject(): any {
        return {
            title: 'TCE-RJ',
            message: 'Encontramos uma falha interna ao realizar esta operação.'
        };
    }

}
