import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';

import { UserManager, UserManagerSettings, User } from 'oidc-client';
import { environment } from '../../environments/environment';

@Injectable()
export class AuthService {
    private manager = new UserManager(getClientSettings());
    public user: User = null;

    constructor() {
        this.updateUserInfo();

        this.manager.events.addUserLoaded(() => {
            this.updateUserInfo();
        });
    }

    updateUserInfo(): void {
        this.manager.getUser().then(user => {
            this.user = user;
        });
    }

    isLoggedIn(): Observable<boolean> {
        return from(this.manager.getUser()).pipe(
            map<User, boolean>((user) => {
                // tslint:disable-next-line:triple-equals
                if (this.user != user) {
                    this.user = user;
                }

                if (user) { return true; } else { return false; }
            })
        );
    }

    getClaims(): any {
        if (this.user == null) {
            return null;
        }

        return this.user.profile;
    }

    getAuthorizationHeaderValue(): string {
        return `${this.user.token_type} ${this.user.access_token}`;
    }

    startAuthentication(): Promise<any> {
        return this.manager.signinRedirect();
    }
}

export function getClientSettings(): UserManagerSettings {
    return {
        authority: environment.auth.authorityUrl,
        client_id: environment.auth.client_id,
        redirect_uri: environment.auth.redirect_uri,
        post_logout_redirect_uri: environment.auth.post_logout_redirect_uri,
        response_type: 'id_token token',
        scope: environment.auth.scope,
        filterProtocolClaims: true,
        loadUserInfo: true,
        automaticSilentRenew: true,
        accessTokenExpiringNotificationTime: 30,
        silent_redirect_uri: environment.auth.silent_redirect_uri
    };
}
