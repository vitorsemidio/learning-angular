export class Processo  {
  id: number;
  status: string;
  totalDePaginas: number;
  porcentagem: number;
  paginasConcluidas: number;
  posicao: number;
  nomeArquivo: string;
  dataDeCriacao: string;
  dataDeTermino: string;
  codigoProcessamento: string;
  hash: string;
}
