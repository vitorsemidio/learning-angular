import { ApiService } from '../services/api.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OcrService {

  constructor(
    private api: ApiService
  ) { }

  obterProcessos() {
    // console.log('servico ocr');
    return this.api.get('lista-grid');
  }

  // criarPost(post: PostModel) {
  //   return this.api.post(`posts`, post);
  // }

  // atualizarPost(post: PostModel) {
  //   return this.api.put(`posts/${post.id}`, post);
  // }

  obterPostPorId(postId: string) {
    return this.api.get(`posts/${postId}`);
  }

  excluirPost(postId: string) {
    return this.api.delete(`posts/${postId}`);
  }
}
