import { ApiService } from './../services/api.service';
import { Processo } from './ocr.model';
import { Component, OnInit } from '@angular/core';
import { OcrService } from './ocr.service';
import { BlockUi } from 'ngx-ui-hero';

@Component({
  selector: 'app-ocr',
  templateUrl: './ocr.component.html',
  styleUrls: ['./ocr.component.scss']
})
export class OcrComponent implements OnInit {
  blockUi: BlockUi;
  processos: Processo[];
  x;

  constructor(
    private ocrService: OcrService,
    private api: ApiService
  ) { }

  ngOnInit() {
    // this.carregarTela();
  }

  carregarTela(): void {
    // this.blockUi.start('Carregando meus posts...');

    Promise.all([
      this.carregarProcessos(),
    ])
    .then(() => {
      console.log('sucesso ao carregar tela');
      // this.blockUi.stop();
    })
    .catch(error => {
      // this.tratarErro(error);
      console.log('erro ao carregar tela');
      // this.blockUi.stop();
    });
  }


  private carregarProcessos(): Promise<void> {
    const promise = new Promise<void>((resolve, reject) => {
      this.ocrService.obterProcessos()
        .subscribe(
          (result: Processo[]) => {
            // console.log(result);
            this.processos = result;
            console.log(this.processos);
            // resolve();
          },
          error => {
            console.log('error ao carregar processos');
            reject();
          }
        );
    });

    return promise;
  }

  obterProcessos(arquivo) {
    var formData = new FormData(arquivo[0]);
    // console.log('servico ocr');
    console.log(formData);
  }



}
