import { AlertService } from 'ngx-ui-hero';


export abstract class BaseComponent {
  constructor(
    public alert: AlertService
  ) { }

  tratarErro(error: any): void {
    if (!error) {
      return;
    }

    if (error.unauthorized) {
      this.alert.warning('Atencao', 'Acesso negado');
      return;
    }

    this.alert.error(error.title, error.message);
  }

}
