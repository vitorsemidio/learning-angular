import { ComentarioCadastroModalComponent } from './../comentario-cadastro-modal/comentario-cadastro-modal.component';
import { AlertService, BlockUi, DataGridColumnModel, EnumAlignment } from 'ngx-ui-hero';
import { finalize, delay, tap } from 'rxjs/operators';

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { PostModel } from '../models/post.model';
import { PostsService } from '../posts.service';
import { BaseComponent } from 'src/app/base/base-component';
import { CommentModel } from '../models/comment.model';
import { BsModalService } from 'ngx-bootstrap';

@Component({
  selector: 'app-post-cadastro',
  templateUrl: './post-cadastro.component.html',
  styleUrls: ['./post-cadastro.component.scss']
})
export class PostCadastroComponent extends BaseComponent implements OnInit {
  blockUi = new BlockUi();
  blockUiComentarios = new BlockUi();
  post: PostModel;

  dadosComentarios: Array<CommentModel>;
  colunasComentarios: Array<DataGridColumnModel> = [
    {
      caption: 'Texto',
      data: 'texto',
      sortable: false
    },
    {
      caption: 'Data de criacao',
      data: 'dataCadastro',
      sortable: false
    },
  ];

  constructor(
    public alert: AlertService,
    private service: PostsService,
    private router: Router,
    private route: ActivatedRoute,
    private modalService: BsModalService
  ) {
    super(alert);
    this.post = new PostModel();
  }

  ngOnInit() {
    this.post.id = this.route.snapshot.params.id;
    this.carregarTela();
  }

  salvar() {
    this.alert.question('Confirmação', 'Você deseja realmente salvar?', () => {
      if (this.post.id) {
        this.atualizarPost();
      } else {
        this.criarPost();
      }
    });
  }

  private carregarTela() {
    this.blockUi.start('Carregando...');

    Promise.all([
      this.carregarCadastro(),
      this.carregarComentarios(),
    ])
    .then(() => {
      this.blockUi.stop();
    })
    .catch(error => {
      this.tratarErro(error);
      this.blockUi.stop();
    });
  }
  private carregarCadastro(): Promise<void> {
    const p = new Promise<void>((resolve, reject) => {
      if (!this.post.id) {
        resolve();
      } else {
        this.service.obterPostPorId(this.post.id)
          .subscribe(
            result => {
              this.post = result;
              resolve();
            },
            error => reject(error)
          );
      }
    });

    return p;
  }

  private carregarComentarios(): Promise<void> {
    const p = new Promise<void>((resolve, reject) => {
      if (!this.post.id) {
        resolve();
      } else {
        this.blockUiComentarios.start('Carregando os comenatios...');
        this.service.obterComentariosPorPost(this.post.id)
          .pipe(finalize(() => this.blockUiComentarios.stop()))
          .subscribe(
            result => {
              this.dadosComentarios = result;
              resolve();
            },
            error => reject(error)
          );
      }
    });

    return p;
  }

  private criarPost() {
    this.blockUi.start('Salvando...');

    // coloca os comentarios salvos em memoris no posts.
    this.post.comments = this.dadosComentarios;

    this.service.criarPost(this.post).pipe(delay(1000))
      .pipe(finalize(() => this.blockUi.stop()))
      .subscribe(
        result => {
          this.alert.success('Feedback', 'Post criado com sucesso!');
          this.router.navigate(['/posts']);
        },
        error => this.tratarErro(error)
      );
  }

  private atualizarPost() {
    this.blockUi.start('Atualizando...');

    this.service.atualizarPost(this.post).pipe(delay(1000))
      .pipe(finalize(() => this.blockUi.stop()))
      .subscribe(
        result => {
          this.alert.success('Feedback', 'Post atualizado com sucesso!');
          this.router.navigate(['/posts']);
        },
        error => this.tratarErro(error)
      );
  }

  abrirCriacaoOuEdicaoDeComentario(index?: number) {
    // indice opcional
    const modalRef = this.modalService.show(ComentarioCadastroModalComponent, {
      class: 'modal-lg',
      initialState: {
        postId: this.post.id,
        comentario: index >= 0 ? this.dadosComentarios[index] : null
      }
    });

    modalRef.content.salvouEmMemoria.subscribe(comentario => {
      if (index >= 0) {
        // editando comentario
        this.dadosComentarios[index] = comentario;
      } else {
        // adicionando comentario
        if (!this.dadosComentarios) {
          this.dadosComentarios = new Array<CommentModel>();
        }
        this.dadosComentarios.push(comentario);
      }
    });


    modalRef.content.persistiu.subscribe(() => {
      //
      this.carregarComentarios();
    });
  }

  excluir(index?: number) {
    this.alert.question('Confirmacao', 'Voce deseja realmente excluir?', () => {
      if (this.post.id) {
        // persistir
        this.blockUiComentarios.start('Excluindo...');

        this.service.excluirComentario(this.post.id, this.dadosComentarios[index].id)
          .pipe(finalize(() => this.blockUiComentarios.stop()))
          .subscribe(
            result => {
              this.alert.success('Feedback', 'Comentario excluido com sucesso');
              this.carregarComentarios();
            },
            error => this.tratarErro(error)
          );
      } else {
        // excluir em memoria
        this.dadosComentarios.splice(index, 1);
      }
    });
  }




}
