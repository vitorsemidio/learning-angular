import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostCadastroComponent } from './post-cadastro.component';

describe('PostCadastroComponent', () => {
  let component: PostCadastroComponent;
  let fixture: ComponentFixture<PostCadastroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostCadastroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostCadastroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
