import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { SharedModule } from '../shared/shared.module';
import { PostsComponent } from './posts.component';
import { PostsRoutingModule } from './posts.routing.module';
import { PostsService } from './posts.service';
import { PostCadastroComponent } from './post-cadastro/post-cadastro.component';
import { ComentarioCadastroModalComponent } from './comentario-cadastro-modal/comentario-cadastro-modal.component';

@NgModule({
  declarations: [PostsComponent, PostCadastroComponent, ComentarioCadastroModalComponent],
  imports: [
    CommonModule,
    FormsModule,
    PostsRoutingModule,
    SharedModule
  ],
  entryComponents: [
    ComentarioCadastroModalComponent
  ],
  providers: [
    PostsService
  ]
})
export class PostsModule { }
