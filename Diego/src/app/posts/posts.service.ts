import { CommentModel } from './models/comment.model';
import { CursoModel } from './models/curso.model';
import { Injectable } from '@angular/core';

import { ApiService } from '../services/api.service';
import { PostModel } from './models/post.model';

@Injectable()
export class PostsService {

  constructor(private api: ApiService) { }

  obterPosts(q: string) {
    return this.api.get(`posts?q=${q}`);
  }

  criarPost(post: PostModel) {
    return this.api.post(`posts`, post);
  }

  atualizarPost(post: PostModel) {
    return this.api.put(`posts/${post.id}`, post);
  }

  obterPostPorId(postId: string) {
    return this.api.get(`posts/${postId}`);
  }

  excluirPost(postId: string) {
    return this.api.delete(`posts/${postId}`);
  }





  // metodos para comentarios
  obterComentariosPorPost(postId: string) {
    return this.api.get(`posts/${postId}/comments`);
  }

  adicionarComentario(postId: string, comment: CommentModel) {
    return this.api.post(`posts/${postId}/comments`, comment);
  }

  atualizarComentario(postId: string, comment: CommentModel) {
    return this.api.put(`posts/${postId}/comments/${comment.id}`, comment);
  }

  excluirComentario(postId: string, commentId: string) {
    return this.api.delete(`posts/${postId}/comments/${commentId}`);
  }

  obterComentarioPorId(postId: string, commentId: string) {
    return this.api.get(`posts/${postId}/comments/${commentId}`);
  }

}
