import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PostCadastroComponent } from './post-cadastro/post-cadastro.component';
import { PostsComponent } from './posts.component';

const routes: Routes = [
    { path: '', component: PostsComponent },
    { path: 'novo', component: PostCadastroComponent },
    { path: ':id/edit', component: PostCadastroComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PostsRoutingModule { }
