import { BaseComponent } from 'src/app/base/base-component';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { PostsService } from './../posts.service';
import { CommentModel } from './../models/comment.model';
import { BlockUi, AlertService } from 'ngx-ui-hero';
import { Component, OnInit, EventEmitter } from '@angular/core';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-comentario-cadastro-modal',
  templateUrl: './comentario-cadastro-modal.component.html',
  styleUrls: ['./comentario-cadastro-modal.component.scss']
})
export class ComentarioCadastroModalComponent extends BaseComponent implements OnInit {
  blockUi = new BlockUi;
  comment: CommentModel;
  postId: string;
  editando: boolean;
  persistiu = new EventEmitter();
  salvouEmMemoria = new EventEmitter<CommentModel>();

  constructor(
    private service: PostsService,
    private modalService: BsModalService,
    private modalRef: BsModalRef,
    public alert: AlertService
  ) {
    super(alert);
  }

  ngOnInit() {
    this.postId = this.modalService.config.initialState['postId'];

    // tslint:disable-next-line:prefer-const
    let comentarioDaTela = this.modalService.config.initialState['comentario'];
    // tslint:disable-next-line:triple-equals
    this.editando = comentarioDaTela != null && comentarioDaTela != undefined;

    if (comentarioDaTela) {
      this.comment = Object.assign({}, comentarioDaTela);
    } else {
      this.comment = new CommentModel();
    }
  }

  salvar() {
    if (this.editando) {
      this.atualizar();
    } else {
      this.criar();
    }
  }

  private criar() {
    if (this.postId) {
      this.blockUi.start('Salvando...');

      this.service.adicionarComentario(this.postId, this.comment)
        .pipe(finalize(() => this.blockUi.stop()))
        .subscribe(
          result => {
            this.alert.success('Feedback', 'Comentario criado com sucesso');
            this.persistiu.emit();
            this.modalRef.hide();
          },
          error => this.tratarErro(error)
        );
    } else {
      this.salvouEmMemoria.emit(this.comment);
      this.modalRef.hide();
    }
  }

  private atualizar() {
    if (this.postId) {
      this.blockUi.start('Salvando...');

      this.service.atualizarComentario(this.postId, this.comment)
        .pipe(finalize(() => this.blockUi.stop()))
        .subscribe(
          result => {
            this.alert.success('Feedback', 'Comentario atualizado com sucesso');
            this.persistiu.emit();
            this.modalRef.hide();
          },
          error => this.tratarErro(error)
        );
    } else {
      this.salvouEmMemoria.emit(this.comment);
      this.modalRef.hide();
    }
  }

}
