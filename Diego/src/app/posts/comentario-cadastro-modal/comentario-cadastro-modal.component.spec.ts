import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComentarioCadastroModalComponent } from './comentario-cadastro-modal.component';

describe('ComentarioCadastroModalComponent', () => {
  let component: ComentarioCadastroModalComponent;
  let fixture: ComponentFixture<ComentarioCadastroModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComentarioCadastroModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComentarioCadastroModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
