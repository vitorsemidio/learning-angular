import { BaseComponent } from 'src/app/base/base-component';
import { AlertService, BlockUi, DataGridColumnModel } from 'ngx-ui-hero';
import { finalize } from 'rxjs/operators';

import { Component, OnInit } from '@angular/core';

import { PostsService } from './posts.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent extends BaseComponent implements OnInit {
  // tslint:disable-next-line:no-inferrable-types
  busca: string = '';
  blockUi = new BlockUi();
  // tslint:disable-next-line:no-inferrable-types
  titulo: string = 'Curso de Angular';
  posts: Array<any>;

  columns: Array<DataGridColumnModel> = [
    {
      caption: 'Título',
      data: 'titulo',
    },
    {
      caption: 'Descrição',
      data: 'descricao',
    },
  ];

  constructor(
    private postsService: PostsService,
    public alert: AlertService
  ) {
    super(alert);
  }

  ngOnInit() {
    this.carregarTela();
  }

  excluir(postId: string) {
    this.alert.question('Confirmação', 'deseja realmente excluir?', () => {
      this.blockUi.start('Excluindo...');

      this.postsService.excluirPost(postId)
        .pipe(finalize(() => this.blockUi.stop()))
        .subscribe(
          result => {
            this.alert.success('Feedback', 'Excluído com sucesso!');
            this.carregarTela();
          },
          error => this.tratarErro(error)
        );
    });
  }

  carregarTela(): void {
    this.blockUi.start('Carregando meus posts...');

    Promise.all([
      this.carregarPosts(),
    ])
    .then(() => {
      this.blockUi.stop();
    })
    .catch(error => {
      this.tratarErro(error);
      this.blockUi.stop();
    });
  }
  private carregarPosts(): Promise<void> {
    const promise = new Promise<void>((resolve, reject) => {
      this.postsService.obterPosts(this.busca)
        .subscribe(
          result => {
            // console.log(result);
            this.posts = result;
            resolve();
          },
          error => reject()
        );
    });

    return promise;
  }

}
