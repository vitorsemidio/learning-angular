import { BaseModel } from './base-model';
import { CommentModel } from './comment.model';

export class PostModel extends BaseModel {
    titulo: string;
    descricao: string;
    comments: Array<CommentModel>;
}
