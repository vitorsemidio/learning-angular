import { BaseModel } from './base-model';

export class CommentModel extends BaseModel {
    texto: string;
    postId: string;
}
